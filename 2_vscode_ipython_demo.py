## vscode can run cells similar to ipython but with a loss less overhead
## and the code is still valid python just with comment lines throughout:

# %%
# This will now be a "cell" until the next # %% appears at the start of a new line

# %%
print("Hi")

# %%
?

# %%
%%timeit
list(range(10))
list(range(100))

# %%
# debug has a couple options in this mode. 
# lets define the same functions from the notebook
def subfunc(x):
    return 1.0/(x-1)

def errorfunc(y):
    return subfunc(y+1)

# %%
# call the function and check it works first
errorfunc(10)


# %%
for x in range(-5, 5):
    errorfunc(x)


# %% can type %debug in a new cell, 
# or in interactive box on the right

# That'll drop you into text mode pdb which is fine...
# Better, is to click "Debug Cell" above the problem cell


