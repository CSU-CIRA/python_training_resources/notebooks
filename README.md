## Introduction to Notebooks

This heavily leans on Jupyter Intro notebooks, We cut out some features for the sake of brevity see here for more details:
https://github.com/ipython/ipython-in-depth

(Also cut some "useful" but frankly questionable features.  IMO, don't become too reliant on notebook magic.)

We'll also talk about interactive cells in VSCode: See [here for more info](https://code.visualstudio.com/docs/python/jupyter-support-py)


## Getting Started:
```
git clone https://gitlab.com/CSU-CIRA/python_training_resources/notebooks.git
cd notebooks
conda env create -f environment.yml
conda activate notebooks
code .   # if you have vscode 
```

You can also view notebooks through the gitlab / github browser websites, or through nbviewer.jupyter.org (this was down when I prepped this however). 


## Opening notebooks in VSCode

VS Code will support notebooks inline, and will run the jupyter server for you behind the scenes.  It will also link to remote servers if you have one available. See the above link for more info.   `1_Beyond_Plain_python.ipynb` includes a short demo of some key jupyter / ipython usage. 


## Interactive ipython cells in VSCode
VS code also includes interactive cells in ipython through special comment lines of `# %%`. `2_vscode_ipython_demo.py` provides a short demo of these functionality.  


## Opening notebooks without VSCode

Our environment includes the tools to run our own jupyter server locally. Start the server and open the notebook with:
`jupyter notebook 1_Beyond_Plain_Python.ipynb`

This will run a local server on port `8888` or similar and should open a browser window or provide a link to manually open.  


## Using Jupyter Lab
Jupyter lab is a server environment, to host whole workspaces, multiple environments, multiple notebooks and interactive terminal sessions.  e.g. You could run a jupyterlab instance on your CIRA machine and can connect through the browser to everything on the remote machine as if it was local.  Security and port forwarding at your own risk.  
